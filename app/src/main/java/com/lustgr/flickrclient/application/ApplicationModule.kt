package com.lustgr.flickrclient.application

import com.googlecode.flickrjandroid.Flickr
import com.lustgr.flickrclient.services.FlickBasedSearchService
import com.lustgr.flickrclient.services.PicassoBasedImageLoader
import com.lustgr.flickrclient.services.interfaces.ImageLoader
import com.lustgr.flickrclient.services.interfaces.SearchService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton


val ApplicationModule = Kodein.Module("application_module") {
    bind<Flickr>() with singleton { Flickr("84434e44ac54eb2853b6b4492daf863e") }
    bind<SearchService>() with singleton { FlickBasedSearchService(instance()) }
    bind<ImageLoader>() with singleton { PicassoBasedImageLoader(instance()) }
}

