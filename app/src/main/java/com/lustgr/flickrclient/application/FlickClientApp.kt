package com.lustgr.flickrclient.application

import android.app.Application
import android.content.Context
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

class FlickClientApp: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        bind<Context>() with singleton { this@FlickClientApp }
        import(ApplicationModule)
    }
}

