package com.lustgr.flickrclient.details

import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.transition.Transition
import android.view.MenuItem
import android.widget.ImageView
import com.lustgr.flickrclient.databinding.ActivityDetailsBinding
import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.services.interfaces.ImageLoader
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance

//TODO тут надо все переписать в MVVM подходе но на это уже нет сил и времени
class DetailsActivity: AppCompatActivity(), KodeinAware{

    companion object {
        const val KEY_SEARCH_ITEM = "SearchItem"
        const val KEY_HEADER_VIEW = "HeaderView"
    }

    private val _parentKodein by closestKodein()

    override val kodein  = Kodein.lazy {
        extend(_parentKodein)
        import(detailsModule)
    }

    val loader: ImageLoader by kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityDetailsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val data = intent.getParcelableExtra<SearchItem>(KEY_SEARCH_ITEM)

        ViewCompat.setTransitionName(binding.detailsPhoto, KEY_HEADER_VIEW)
        loader.loadImageToView(binding.detailsPhoto, data.thumbnailUrl)

        addTransitionListener(binding.detailsPhoto, data.photoUrl)

        supportActionBar?.title = data.title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addTransitionListener(view: ImageView, url:String): Boolean {
        val transition = window.sharedElementEnterTransition

        if (transition != null) {
            transition.addListener(object : Transition.TransitionListener {
                override fun onTransitionEnd(transition: Transition) {
                    loader.loadImageSilently(view, url)
                    transition.removeListener(this)
                }

                override fun onTransitionStart(transition: Transition) {
                    // No-op
                }

                override fun onTransitionCancel(transition: Transition) {
                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this)
                }

                override fun onTransitionPause(transition: Transition) {
                    // No-op
                }

                override fun onTransitionResume(transition: Transition) {
                    // No-op
                }
            })
            return true
        }

        // If we reach here then we have not added a listener
        return false
    }
}

val detailsModule = Kodein.Module("detailsModule"){

}
