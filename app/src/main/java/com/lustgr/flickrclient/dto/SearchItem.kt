package com.lustgr.flickrclient.dto

import android.os.Parcel
import android.os.Parcelable

//TODO лучше не использовать этот клас для отображания списка найденых предметов
//надо создать SearchItemViewModel и мапить SearchItem to SearchItemViewModel
//внутри SearchModel
class SearchItem(
        val title:String,
        val description:String,
        val thumbnailUrl: String,
        val photoUrl:String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeString(description)
        parcel.writeString(thumbnailUrl)
        parcel.writeString(photoUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchItem> {
        override fun createFromParcel(parcel: Parcel): SearchItem {
            return SearchItem(parcel)
        }

        override fun newArray(size: Int): Array<SearchItem?> {
            return arrayOfNulls(size)
        }
    }
}