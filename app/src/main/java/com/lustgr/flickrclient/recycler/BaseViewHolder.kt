package com.lustgr.flickrclient.recycler

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder<TItemViewModel>(root: View):RecyclerView.ViewHolder(root){
    abstract fun bind(item:TItemViewModel)
}

