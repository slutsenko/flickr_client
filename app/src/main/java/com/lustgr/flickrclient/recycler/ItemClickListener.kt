package com.lustgr.flickrclient.recycler

import android.view.View

interface ItemClickListener<TItemViewModel>{
    fun onClickItem(view: View, item: TItemViewModel)
}