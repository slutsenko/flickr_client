package com.lustgr.flickrclient.recycler

import android.databinding.ObservableArrayList
import android.databinding.ObservableList
import android.support.v7.widget.RecyclerView

abstract class ObservableArrayAdapter<TItemViewModel>(
        protected val items: ObservableArrayList<TItemViewModel>
    ): RecyclerView.Adapter<BaseViewHolder<TItemViewModel>>() {

    init {
        items.addOnListChangedCallback(object: ObservableList.OnListChangedCallback<ObservableList<*>?>() {
            override fun onChanged(sender: ObservableList<*>?) {
                notifyDataSetChanged()
            }

            override fun onItemRangeRemoved(sender: ObservableList<*>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeRemoved(positionStart, itemCount)
            }

            override fun onItemRangeMoved(sender: ObservableList<*>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
                notifyItemRangeRemoved(fromPosition, itemCount)
            }

            override fun onItemRangeInserted(sender: ObservableList<*>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeInserted(positionStart, itemCount)
            }

            override fun onItemRangeChanged(sender: ObservableList<*>?, positionStart: Int, itemCount: Int) {
                notifyItemRangeChanged(positionStart, itemCount)
            }
        })
    }

    override fun onBindViewHolder(holder: BaseViewHolder<TItemViewModel>, position: Int) {
        holder.bind(items[position])
    }
}