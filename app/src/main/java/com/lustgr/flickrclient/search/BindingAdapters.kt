package com.lustgr.flickrclient.search

import android.databinding.BindingAdapter
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import com.lustgr.flickrclient.services.interfaces.ImageLoader

@BindingAdapter("init")
fun initSearchScreen(recyclerView: RecyclerView, viewModel:SearchViewModel){

    if(recyclerView.adapter == null) {
        val items = viewModel.photos

        recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 2)
        recyclerView.adapter = SearchAdapter(items, viewModel)

        recyclerView.addOnScrollListener(object: RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val mLayoutManager =  recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount = mLayoutManager.childCount
                val totalItemCount = mLayoutManager.itemCount
                val pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition()

                if (pastVisibleItems + visibleItemCount + 6 >= totalItemCount) {
                    viewModel.loadNextPage()
                }
            }
        })
    }
}

@BindingAdapter("url", "loader")
fun loadImageToUrl(view: ImageView, url:String?, loader: ImageLoader){
    if(!url.isNullOrEmpty()){
        loader.loadImageToView(view, url!!)
    }
}
