package com.lustgr.flickrclient.search

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.lustgr.flickrclient.databinding.ActivitySearchBinding
import com.lustgr.flickrclient.details.DetailsActivity
import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.recycler.ItemClickListener
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import org.kodein.di.generic.instance


class SearchActivity: AppCompatActivity(), KodeinAware, ItemClickListener<SearchItem> {

    private val _parentKodein by closestKodein()

    override val kodein  = Kodein.lazy {
        extend(_parentKodein)
        import(SearchModule(this@SearchActivity).create())
    }

    private val viewModel: SearchViewModel by kodein.instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySearchBinding.inflate(layoutInflater)
        binding.vm = viewModel
        setContentView(binding.root)
    }

    //TODO было бы правильнее создать NavigationService, реализовать внутри него логику переходов между экранами,
    //инжектить его во ViewModel или Model и там ообрабатывать onClickItem.
    //нет времени заниматься этим в рамках тестового задания
    override fun onClickItem(view:View, item: SearchItem) {
        // Construct an Intent as normal
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtra(DetailsActivity.KEY_SEARCH_ITEM, item)

        val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                android.support.v4.util.Pair<View, String>(view, DetailsActivity.KEY_HEADER_VIEW)
        )
        ActivityCompat.startActivity(this, intent, activityOptions.toBundle())
    }
}

