package com.lustgr.flickrclient.search

import android.databinding.ObservableArrayList
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lustgr.flickrclient.databinding.ItemPhotoBinding
import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.recycler.BaseViewHolder
import com.lustgr.flickrclient.recycler.ObservableArrayAdapter

class SearchAdapter(items: ObservableArrayList<SearchItem>, private val viewModel:SearchViewModel): ObservableArrayAdapter<SearchItem>(items) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<SearchItem> {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPhotoBinding.inflate(inflater, parent, false)

        binding.itemClickListener = viewModel.itemClickListener
        binding.imageLoader = viewModel.loader

        return SearchItemViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}