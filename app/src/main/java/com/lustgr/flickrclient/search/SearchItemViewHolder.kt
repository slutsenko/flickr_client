package com.lustgr.flickrclient.search

import com.lustgr.flickrclient.databinding.ItemPhotoBinding
import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.recycler.BaseViewHolder

class SearchItemViewHolder(private val binding: ItemPhotoBinding): BaseViewHolder<SearchItem>(binding.root){
    override fun bind(item: SearchItem) {
        binding.vm = item
    }
}