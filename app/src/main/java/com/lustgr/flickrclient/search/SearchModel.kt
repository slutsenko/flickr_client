package com.lustgr.flickrclient.search

import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.recycler.ItemClickListener
import com.lustgr.flickrclient.services.interfaces.ImageLoader
import com.lustgr.flickrclient.services.interfaces.SearchService
import com.lustgr.flickrclient.services.interfaces.SearchingQueryService
import io.reactivex.Observable

class SearchModel(
        private val searchService: SearchService,
        val loader:ImageLoader,
        val itemClickListener: ItemClickListener<SearchItem>,
        val searchingQueryService: SearchingQueryService
){

    private val firstPage = 1
    private val perPage = 30

    private var currentPage = firstPage
    private var currentQuery = ""


    fun getNextPageForQuery(query:String): Observable<List<SearchItem>> {

        if(currentQuery != query) {
            currentPage = firstPage
            currentQuery = query
        }

        return searchService.search(currentQuery, currentPage++, perPage)
    }
}