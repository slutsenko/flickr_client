package com.lustgr.flickrclient.search

import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.recycler.ItemClickListener
import com.lustgr.flickrclient.services.DumbSearchingQueryService
import com.lustgr.flickrclient.services.interfaces.SearchingQueryService
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class SearchModule(private val itemClickListener: ItemClickListener<SearchItem>) {

    fun create(): Kodein.Module {
        return Kodein.Module("searchModule") {
            bind<ItemClickListener<SearchItem>>() with instance(itemClickListener)
            bind<SearchModel>() with provider { SearchModel(instance(), instance(), instance(), instance()) }
            bind<SearchViewModel>() with provider { SearchViewModel(instance()) }
            bind<SearchingQueryService>() with singleton { DumbSearchingQueryService() }
        }
    }
}