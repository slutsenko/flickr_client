package com.lustgr.flickrclient.search

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import com.lustgr.flickrclient.dto.SearchItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class SearchViewModel(private val searchModel:SearchModel) {

    val query = ObservableField<String>("")

    private var searchDisposable:Disposable? = null

    val loading = ObservableField<Boolean>(false)

    val photos = ObservableArrayList<SearchItem>()

    //TODO it is looks a bit ugly
    val loader = searchModel.loader
    val itemClickListener = searchModel.itemClickListener
    private val queriesService = searchModel.searchingQueryService

    private val querySubject = PublishSubject.create<String>()

    init {
        query.addOnPropertyChangedCallback(object: android.databinding.Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: android.databinding.Observable?, propertyId: Int) {
                querySubject.onNext(query.get()!!)
            }
        })

        querySubject
                .throttleLast(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    photos.clear()
                    loadPhotosForQuery(it)
                }
    }

    private fun loadPhotosForQuery(query: String){
        loading.set(true)
        if(searchDisposable != null && !(searchDisposable!!.isDisposed)){
            searchDisposable!!.dispose()
        }

        searchDisposable = searchModel.getNextPageForQuery(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            loading.set(false)
                            photos.addAll(it)
                            if(it.isNotEmpty()){
                               queriesService.addQuery(query)
                            }
                        },
                        {
                            loading.set(false)
                        }
                )
    }

    fun loadNextPage(){
        if(loading.get()!!){
            return
        }

        loadPhotosForQuery(query.get()!!)
    }
}