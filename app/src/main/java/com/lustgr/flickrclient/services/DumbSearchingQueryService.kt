package com.lustgr.flickrclient.services

import com.lustgr.flickrclient.services.interfaces.SearchingQueryService

class DumbSearchingQueryService: SearchingQueryService {

    private val maxQuery = 3

    var queries = ArrayList<String>()

    override fun addQuery(query: String) {
        if(queries.contains(query)){
            queries.remove(query)
        } else if(queries.size>=maxQuery){
            queries.removeAt(queries.size - 1)
        }
        queries.add(0, query)
    }

    override fun getLatestQueries(): List<String> {
        return queries
    }
}