package com.lustgr.flickrclient.services

import com.googlecode.flickrjandroid.Flickr
import com.googlecode.flickrjandroid.photos.PhotosInterface
import com.googlecode.flickrjandroid.photos.SearchParameters
import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.services.interfaces.SearchService
import io.reactivex.Observable

//TODO inject a ThumbnailSizeProvider that will provide size of thumbnail in according to phone screen size
class FlickBasedSearchService(flickr:Flickr): SearchService {
    private val photosInterface: PhotosInterface = flickr.photosInterface

    override fun search(query: String, page: Int, perPage: Int): Observable<List<SearchItem>> {
         return Observable.just(SearchData(query, page, perPage))
                .map{
                    if(it.query.isEmpty()) {
                        photosInterface.getRecent(HashSet<String>(), it.perPage, it.page)
                    } else {
                        val params = SearchParameters()
                        params.text = query
                        photosInterface.search(params, it.perPage, it.page)
                    }
                }
                .map {
                    it.map { SearchItem(it.title.orEmpty(),
                                it.description.orEmpty(),
                                it.small320Url.orEmpty(),
                                it.large1600Url.orEmpty()
                    )}
                }
    }

    private class SearchData(val query: String, val page: Int, val perPage: Int)
}