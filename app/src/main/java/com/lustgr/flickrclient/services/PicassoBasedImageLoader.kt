package com.lustgr.flickrclient.services

import android.app.Activity
import android.content.Context
import android.os.Environment
import android.widget.ImageView
import com.lustgr.flickrclient.BuildConfig
import com.lustgr.flickrclient.R
import com.lustgr.flickrclient.services.interfaces.ImageLoader
import com.squareup.picasso.Callback
import com.squareup.picasso.LruCache
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import java.lang.Exception

class PicassoBasedImageLoader(context:Context): ImageLoader {

    private val picasso: Picasso = Picasso.Builder(context)
            .loggingEnabled(BuildConfig.DEBUG)
            .indicatorsEnabled(BuildConfig.DEBUG)
            .memoryCache(LruCache(Int.MAX_VALUE))
            .downloader(OkHttp3Downloader(Environment.getDownloadCacheDirectory(), Long.MAX_VALUE))
            .build()

    override fun loadImageToView(view: ImageView, url: String) {
        picasso.load(url)
            .placeholder(R.drawable.loading_animated)
            .error(R.drawable.ic_download_error)
            .into(view)
    }

    override fun loadImageSilently(view: ImageView, url: String) {
        //TODO should be redone looks like a hack
        picasso.load(url).fetch(object: Callback {
            override fun onSuccess() {
                val activity = view.context as Activity
                if(activity.isFinishing){
                    return
                }
                picasso.load(url).into(view)
            }

            override fun onError(e: Exception?) {

            }
        })
    }

}