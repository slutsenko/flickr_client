package com.lustgr.flickrclient.services.interfaces

import android.widget.ImageView

interface ImageLoader {
    fun loadImageToView(view: ImageView, url:String)

    fun loadImageSilently(view: ImageView, url:String)
}

