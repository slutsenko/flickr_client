package com.lustgr.flickrclient.services.interfaces

import com.lustgr.flickrclient.dto.SearchItem
import io.reactivex.Observable

interface SearchService {
    fun search(query: String, page: Int, perPage:Int): Observable<List<SearchItem>>
}

