package com.lustgr.flickrclient.services.interfaces

interface SearchingQueryService {
    fun getLatestQueries(): List<String>
    fun addQuery(query:String)
}

