package com.lustgr.flickrclient.stub

import com.lustgr.flickrclient.dto.SearchItem
import com.lustgr.flickrclient.services.interfaces.SearchService
import io.reactivex.Observable

class DumbSearchService: SearchService {
    override fun search(query: String, page: Int, perPage: Int): Observable<List<SearchItem>> {
        return Observable.just(List(perPage) {
            val itit = perPage * page + it
            SearchItem("Photo $itit",
                    "Description of photo $itit",
                    "small_url_$itit",
                    "url_$itit")
        })
    }
}